
import java.awt.Color;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ujwal kr. singh UJJU
 */
public class classs extends javax.swing.JFrame {
 Connection conn;//
    Statement stmt;//
    ResultSet rs;//
    String query;//
    /**
     * Creates new form classs
     */
    public classs() {
        initComponents();
         setTitle("ClassPage");
        getContentPane().setBackground(new Color(204,204,255));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        clno1 = new javax.swing.JLabel();
        cp = new javax.swing.JLabel();
        flag = new javax.swing.JLabel();
        clno = new javax.swing.JTextField();
        cap = new javax.swing.JTextField();
        flg = new javax.swing.JTextField();
        update = new javax.swing.JButton();
        delete = new javax.swing.JButton();
        insert = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("classs");

        clno1.setText("class no. :");

        cp.setText("capacity :");

        flag.setText("flag :");

        clno.setText("clno");

        cap.setText("cap");

        flg.setText("flg");

        update.setText("update");
        update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateActionPerformed(evt);
            }
        });

        delete.setText("delete");
        delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteActionPerformed(evt);
            }
        });

        insert.setText("insert");
        insert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                insertActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(166, 166, 166)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cp)
                            .addComponent(clno1)
                            .addComponent(flag)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(clno, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                                    .addComponent(cap)
                                    .addComponent(flg))))))
                .addContainerGap(288, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(insert)
                .addGap(18, 18, 18)
                .addComponent(delete)
                .addGap(18, 18, 18)
                .addComponent(update)
                .addGap(93, 292, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(55, 55, 55)
                .addComponent(clno1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(clno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(cp)
                .addGap(4, 4, 4)
                .addComponent(cap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(flag)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(flg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(insert)
                    .addComponent(delete)
                    .addComponent(update))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteActionPerformed
        try {
            String sql;
            String clnum = clno.getText();
            String size= cap.getText();
            String fllg= flg.getText();
          
            // TODO add your handling code here:
            Class.forName("java.sql.Driver");//
            conn = DriverManager.getConnection("jdbc:mysql://localhost/project321");//
            stmt=conn.createStatement();//
            //query="Select * from employeeinfo";//
            //rs=stmt.executeQuery(query);//
            
             sql = "delete from class where clno='"+clnum+"';";
                               stmt.executeUpdate(sql);
            
            //JOptionPane.showMessageDialog (null, "Connection Established");
            
            
    }
 catch (ClassNotFoundException | SQLException ex) {
            
            JOptionPane.showMessageDialog(null, ex);//
        } // TODO add your handling code here:
    }//GEN-LAST:event_deleteActionPerformed

    private void insertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_insertActionPerformed
try {
            String sql;
            String clnum = clno.getText();
            String size= cap.getText();
            String fllg= flg.getText();
          
            // TODO add your handling code here:
            Class.forName("java.sql.Driver");//
            conn = DriverManager.getConnection("jdbc:mysql://localhost/project321");//
            stmt=conn.createStatement();//
            //query="Select * from employeeinfo";//
            //rs=stmt.executeQuery(query);//
            
             sql = "INSERT INTO class(clno,cap,flg) VALUES('"+clnum+"',"+size+","+fllg+");";
                               stmt.executeUpdate(sql);
            
            //JOptionPane.showMessageDialog (null, "Connection Established");
            
            
    }
 catch (ClassNotFoundException | SQLException ex) {
            
            JOptionPane.showMessageDialog(null, ex);//
        }        // TODO add your handling code here:
    }//GEN-LAST:event_insertActionPerformed

    private void updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateActionPerformed
try {
            String sql;
            String clnum = clno.getText();
            String size= cap.getText();
            String fllg= flg.getText();
          
            // TODO add your handling code here:
            Class.forName("java.sql.Driver");//
            conn = DriverManager.getConnection("jdbc:mysql://localhost/project321");//
            stmt=conn.createStatement();//
            //query="Select * from employeeinfo";//
            //rs=stmt.executeQuery(query);//
            
             sql = "update class set clno='"+clnum+"',cap="+size+",flg="+fllg+" where clno='"+clnum+"';";
                               stmt.executeUpdate(sql);
            
            //JOptionPane.showMessageDialog (null, "Connection Established");
            
            
    }
 catch (ClassNotFoundException | SQLException ex) {
            
            JOptionPane.showMessageDialog(null, ex);//
        }         // TODO add your handling code here:
    }//GEN-LAST:event_updateActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(classs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(classs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(classs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(classs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new classs().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField cap;
    private javax.swing.JTextField clno;
    private javax.swing.JLabel clno1;
    private javax.swing.JLabel cp;
    private javax.swing.JButton delete;
    private javax.swing.JLabel flag;
    private javax.swing.JTextField flg;
    private javax.swing.JButton insert;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton update;
    // End of variables declaration//GEN-END:variables
}
