/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package routine;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Srutigoyal
 */
public class Routine {

    /**
     * @param args the command line arguments
     */
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/project321";
    // Database credentials
    static final String USER = "root";
    static final String PASS = "root";

    public static void main(String[] args) throws SQLException {
        // TODO code application logic here
        Connection conn = null;
        Statement stmt = null, st = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //STEP 4: Execute a query
            stmt = conn.createStatement();
            String sql, cou = null, rs_deptt[] = new String[50], room = null, cn = null, start = null, c = null;
            ResultSet r;
            int NoOfDeptt = 0, i = 0, x = 1, strength = 0, id;
            sql = "update deptt set countth=theoryhrs, countprac=prachrs, pracflag=0, daily=1;";
            stmt.executeUpdate(sql);
            sql = "update class set flag=0;";
            stmt.executeUpdate(sql);
            sql = "update faculty set flag=0, daily_hrs=0;";
            stmt.executeUpdate(sql);
            sql = "update course set combined_flag=0;";
            stmt.executeUpdate(sql);
            sql = "Select distinct depttname from deptt;";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                rs_deptt[i++] = rs.getString("depttname");
            }
            NoOfDeptt = i;
            int lunch[] = new int[NoOfDeptt];
            int dailyprac[] = new int[NoOfDeptt];
            sql = "select depttname from deptt order by depttname limit 1;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                start = rs.getString("depttname");
            }
            i = Integer.parseInt(start.substring(0, 1));
            System.out.println(i);
            for (; i <= 10; i += 4) {
                c = String.valueOf(i);
                for (int j = 0; j < NoOfDeptt; j++) {
                    if (rs_deptt[j].startsWith(c)) {
                        lunch[j] = 4;
                    }
                }
            }
            i = Integer.parseInt(start.substring(0, 1));
            System.out.println(i);
            for (i = i + 2; i <= 10; i += 4) {
                c = String.valueOf(i);
                for (int j = 0; j < NoOfDeptt; j++) {
                    if (rs_deptt[j].startsWith(c)) {
                        lunch[j] = 5;
                    }
                }
            }
            for (int day = 0; day < 5; day++) 
            //int day = 0;
            {
                sql = "update deptt set deptt.daily=1;";
                stmt.executeUpdate(sql);
                sql = "update faculty set faculty.daily_hrs=0;";
                stmt.executeUpdate(sql);
                for (i = 0; i < NoOfDeptt; i++) {
                    sql = "select sum(prachrs) from deptt where depttname like '" + rs_deptt[i] + "';";
                    rs = stmt.executeQuery(sql);
                    while (rs.next()) {
                        x = Integer.parseInt(rs.getString("sum(prachrs)"));
                    }
                    if (x <= 5) {
                        dailyprac[i] = 1;
                    } else {
                        dailyprac[i] = 2;
                    }
                }
                sql = "drop table if exists day" + day + ";";
                stmt.executeUpdate(sql);
                sql = "Create table day" + day + " (deptts varchar(10), foreign key (deptts) references deptt(depttname) );";
                stmt.executeUpdate(sql);
                sql = "insert into day" + day + " select distinct depttname from deptt;";
                stmt.executeUpdate(sql);
                for (int ts = 0; ts < 9; ts++)//ts= timeslots=9-10, 10-11, 11-11.10, 11.10-12,,,
                {
                    sql = "alter table day" + day + " add ts" + ts + " varchar(10);";
                    stmt.executeUpdate(sql);
                    sql = "alter table day" + day + " add room" + ts + " varchar(20);";
                    stmt.executeUpdate(sql);
                    if (ts != 2) {
                        for (int d = 0; d < NoOfDeptt; d++) {
                            cou=null; room=null;
                            if (ts != lunch[d])//prac 2nd hour
                            {
                                sql = "select ts" + ts + " is null from day" + day + " where deptts='"
                                        + rs_deptt[d] + "';";
                                rs = stmt.executeQuery(sql);
                                while (rs.next()) {
                                    x = Integer.parseInt(rs.getString("ts" + ts + " is null"));
                                }
                                if (x == 1 && ts > 0) {
                                    System.out.println("here" + ts + " " + d);
                                    sql = " select courseno from deptt where depttname='"
                                            + rs_deptt[d] + "' and pracflag=1;";
                                    rs = stmt.executeQuery(sql);
                                    while (rs.next()) {
                                        cou = rs.getString("courseno");
                                    }
                                    System.out.println(cou);
                                    if (cou != null) {
                                        sql = "update day" + day + " set ts" + ts + "='" + cou + "', room" + ts
                                                + "=room" + (ts - 1) + " where day" + day + ".deptts='" + rs_deptt[d] + "';";
                                        stmt.executeUpdate(sql);
                                    }
                                }//prac2nd hour finish
                                cou = null;
                                room = null;
                                //combined already allocated
                                sql = "select ts" + ts + " is null from day" + day + " where deptts='"
                                        + rs_deptt[d] + "';";
                                rs = stmt.executeQuery(sql);
                                while (rs.next()) {
                                    x = Integer.parseInt(rs.getString("ts" + ts + " is null"));
                                }
                                if (x == 1) {
                                    System.out.println("1here" + ts + " " + d);
                                    sql = "select deptt.courseno from deptt, course, combined where deptt.depttname='"
                                            + rs_deptt[d] + "' and deptt.courseno=course.courseno and deptt.theoryhrs>0"
                                            + " and course.combined_flag=1 and deptt.theoryhrs>0 and course.combinedid=combined.combinedid and "
                                            + "combined.depttname='" + rs_deptt[d] + "';";
                                    rs = stmt.executeQuery(sql);
                                    while (rs.next()) {
                                        cou = rs.getString("courseno");
                                    }
                                    if (cou != null) {
                                        sql = "select room" + ts + " from day" + day + " where ts" + ts + "='" + cou + "';";
                                        rs = stmt.executeQuery(sql);
                                        while (rs.next()) {
                                            room = rs.getString("room" + ts);
                                        }

                                        sql = "update day" + day + ", deptt set ts" + ts + "='" + cou
                                                + "', deptt.daily=deptt.daily-1, deptt.countth=deptt.countth-1, room" + ts
                                                + "='" + room + "'where day" + day + ".deptts='" + rs_deptt[d]
                                                + "' and deptt.depttname='" + rs_deptt[d]
                                                + "' and deptt.courseno='" + cou + "';";
                                        stmt.executeUpdate(sql);
                                    }
                                }//combined already allocated finish
                                cou = null;
                                room = null;
                                sql = "select ts" + ts + " is null from day" + day + " where deptts='"
                                        + rs_deptt[d] + "';";
                                rs = stmt.executeQuery(sql);
                                while (rs.next()) {
                                    x = Integer.parseInt(rs.getString("ts" + ts + " is null"));
                                }
                                if (x == 1)//the last 3
                                {
                                    cou=null; room=null;
                                    sql = "select distinct course.courseno, course.combinedid, deptt.countth, deptt.countprac, faculty.Fid\n"
                                            + "from course, class, faculty, deptt, combined\n"
                                            + "where deptt.depttname='" + rs_deptt[d] + "'\n"
                                            + "and course.courseno=deptt.courseno\n"
                                            + "and course.Fid=faculty.Fid\n"
                                            + "and faculty.flag=0\n"
                                            + "order by deptt.countth+deptt.countprac desc;";
                                    rs = stmt.executeQuery(sql);
                                    while (rs.next()) {
                                        cou = rs.getString("courseno");
                                        
                                        id = Integer.parseInt(rs.getString("combinedid"));
                                        int th = Integer.parseInt(rs.getString("countth"));
                                        int pr = Integer.parseInt(rs.getString("countprac"));
                                        String Fid = rs.getString("Fid");
                                        System.out.println(cou + " " + id + " " + th + " " + pr);
                                        strength = 0;
                                        if(id==0)
                                        System.out.println("id==0: ");
                                        //combined theory
                                        System.out.print(cou+" hi ");
                                        if (id != 0 && th > 0) {
                                           sql = "select ifnull((select distinct combined.depttname from class, deptt, combined, course, day"+day+"\n" +
"where deptt.depttname='"+rs_deptt[d]+"' and deptt.courseno='"+cou+"'\n" +
" and combined.combinedid = "+id+"\n" +
" and combined.depttname=day"+day+".deptts\n" +
"  and day"+day+".ts"+ts+" is not null limit 1), '1') as y;";
                                            st = conn.createStatement();
                                            r = st.executeQuery(sql);
                                            while (r.next()) {
                                                start = r.getString("y");
                                            }
                                            //System.out.println(rs_deptt[d]+" Start="+start);
                                            if (start.equals("1")){
                                            sql = "select ifnull((select distinct combined.depttname from class, deptt, combined, course, day"+day+"\n" +
                                            "where combined.combinedid = "+id+"\n" +
                                            " and combined.depttname=deptt.depttname" +
                                            " and deptt.pracflag=1 limit 1), '1') as y;";
                                            st = conn.createStatement();
                                            r = st.executeQuery(sql);
                                            while (r.next()) {
                                                start = r.getString("y");
                                            }
                                            }
                                            if (start.equals("1")) {
                                            start=null;
                                                sql = "select distinct deptt.depttname, deptt.maxstrength from deptt,"
                                                        + " combined where deptt.depttname= combined.depttname\n"
                                                        + "and combined.combinedid='" + id + "';";
                                                r = st.executeQuery(sql);
                                                while (r.next()) {
                                                    if (r.getString("maxstrength") != null) {
                                                        strength += Integer.parseInt(r.getString("maxstrength"));
                                                    }
                                                }
                                                System.out.println(strength);
                                                sql = "select distinct class.classno from class, deptt, combined, course, day" + day
                                                        + " where deptt.depttname='" + rs_deptt[d] + "' and deptt.courseno='" + cou
                                                        + "' and deptt.daily>0 and course.courseno=deptt.courseno and course.combinedid=combined.combinedid\n"
                                                        + "and combined.depttname!='" + rs_deptt[d] + "' and combined.depttname=day" + day
                                                        + ".deptts and day" + day + ".ts" + ts + " is null and class.flag=0\n"
                                                        + "and class.classno not like '%lab' and class.capacity= " + strength
                                                        + " limit 1;";
                                                r = st.executeQuery(sql);
                                                while (r.next()) {
                                                    room = r.getString("classno");
                                                }
                                                System.out.println(cou+ " "+ room);
                                                if(cou!=null && room !=null)
                                                {
                                                    sql = "update day" + day + ", class, faculty, combined, course, deptt\n"
                                                        + "set day" + day + ".ts" + ts + "='" + cou + "', day" + day + ".room" + ts + "='" + room + "',"
                                                        + "  class.flag=1, faculty.flag=1,  deptt.countth=deptt.countth-1, "
                                                        + "course.combined_flag=1, deptt.daily=deptt.daily-1"
                                                        + " where day" + day + ".deptts='" + rs_deptt[d] + "' and course.courseno='" + cou
                                                        + "' and faculty.Fid='"+Fid+"' and deptt.depttname='" + rs_deptt[d]
                                                        + "' and deptt.courseno='" + cou + "' and class.classno='" + room + "';";
                                                    st.executeUpdate(sql);
                                                    System.out.println("2here" + ts + " " + d + rs_deptt[d]);
                                                }
                                            }
                                        }//combined theory ends
                                        //prac 1st hour
                                        sql = "select ts" + ts + " is null from day" + day + " where deptts='"
                                        + rs_deptt[d] + "';";
                                            r = st.executeQuery(sql);
                                            while (r.next()) {
                                                x = Integer.parseInt(r.getString("ts" + ts + " is null"));
                                            }
                                        if(pr>0 && dailyprac[d]>0 && x==1 && ts!=1 && ts!=8 && lunch[d]!=ts+1)
                                        { System.out.println("PRACTICAL");
                                            sql="select distinct deptt.courseno, course.classno\n" +
                                                "from deptt, course, class, faculty\n" +
                                                "where deptt.depttname='"+rs_deptt[d]+"'\n" +
                                                "and deptt.courseno='"+cou+"'\n" +
                                                "and faculty.Fid='"+Fid+"'\n" +
                                                "and faculty.daily_hrs<=5\n and course.courseno='" +cou+
                                                "' and course.classno=class.classno\n" +
                                                "and course.classno!='none'\n" +
                                                "and class.flag=0\n" +
                                                "and class.capacity=deptt.maxstrength\n" +
                                                "limit 1;";
                                            st = conn.createStatement();
                                            r=st.executeQuery(sql);
                                            while(r.next())
                                            room=r.getString("classno");
                                            System.out.println(cou+" "+room);
                                            if(cou!=null && room!=null)
                                            {
                                                sql="update day"+day+", faculty, class, deptt\n" +
                                                "set day"+day+".ts"+ts+"='"+cou+"', day"+day+".room"+ts+"='"+room
                                                +"', faculty.flag=2, class.flag=2, deptt.pracflag=2, "
                                                + "deptt.countprac=deptt.countprac=deptt.countprac-1\n" +
                                                "where day"+day+".deptts='"+rs_deptt[d]+"' and deptt.depttname='"+rs_deptt[d]+"' and deptt.courseno='"+cou
                                                 +"' and class.classno='"+room+"'\n" +
                                                "and faculty.Fid='"+Fid+"' ;";
                                                st.executeUpdate(sql);
                                                dailyprac[d]-=1;
                                            }
                                        }//practical 1st hour finishes
                                        sql = "select ts" + ts + " is null from day" + day + " where deptts='"
                                        + rs_deptt[d] + "';";
                                        r = st.executeQuery(sql);
                                        while (r.next()) {
                                            x = Integer.parseInt(r.getString("ts" + ts + " is null"));
                                        }
                                        if(x==1 && th>0 && id==0)//normal theory
                                        {System.out.println("NORMAL");
                                            sql="select class.classno from deptt, course, faculty, class\n" +
                                            "where deptt.depttname='"+rs_deptt[d]+"' and deptt.courseno='"+cou+"' and deptt.daily>0 " +
                                            "and faculty.Fid='"+Fid+"' and faculty.daily_hrs<=6 and class.flag=0\n" +
                                            "and class.classno not like '%lab' and class.capacity=deptt.maxstrength;";
                                            r=st.executeQuery(sql);
                                            //strength=0;
                                            while(r.next())
                                            {
                                                room=r.getString("classno");
                                                //strength=Integer.parseInt(r.getString("capacity"));
                                            }
                                            if (room==null)
                                            {
                                                sql="select class.classno from deptt, course, faculty, class\n" +
                                            "where deptt.depttname='"+rs_deptt[d]+"' and deptt.courseno='"+cou+"' and deptt.daily>0 " +
                                            "and faculty.Fid='"+Fid+"' and faculty.daily_hrs<=6 and class.flag=0\n" +
                                            "and class.classno not like '%lab' and class.capacity>=deptt.maxstrength;";
                                            r=st.executeQuery(sql);
                                            //strength=0;
                                                while(r.next())
                                                {
                                                    room=r.getString("classno");
                                                //strength=Integer.parseInt(r.getString("capacity"));
                                                }
                                            }
                                            if(room!=null)
                                            {
                                                System.out.println("UPDATED");
                                                sql="update day"+day+", class, faculty, combined, course, deptt\n" +
                                            "set day"+day+".ts"+ts+"='"+cou+"', day"+day+".room"+ts+"='"+room
                                            +"', class.flag=1, faculty.flag=1, deptt.daily=deptt.daily-1, deptt.countth=deptt.countth-1\n" +
                                            "where day"+day+".deptts='"+rs_deptt[d]+"' and course.courseno='"+cou
                                            +"' and course.Fid=faculty.Fid and deptt.depttname='"+rs_deptt[d]
                                                +"' and deptt.courseno='"+cou+"' and class.classno='"+room+"';";
                                            st.executeUpdate(sql);
                                            }
                                        }//normal theory ends
                                        sql = "select ts" + ts + " is null from day" + day + " where deptts='"
                                                + rs_deptt[d] + "';";
                                        r = st.executeQuery(sql);
                                        while (r.next()) {
                                            x = Integer.parseInt(r.getString("ts" + ts + " is null"));
                                        }
                                        if (x == 0) {
                                            break;
                                        }
                                    }
                                }
                            }//if ts is not lunch of this deptt
                        }//loop deptt
                    }//if ts!=2
                    sql = "update class set class.flag=class.flag-1 where class.flag>0;";
                    stmt.executeUpdate(sql);
                    sql = "update faculty set faculty.flag=faculty.flag-1, faculty.daily_hrs=faculty.daily_hrs+1 where faculty.flag>0;";
                    stmt.executeUpdate(sql);
                    sql = "update course set course.combined_flag=course.combined_flag-1 where course.combined_flag>0;";
                    stmt.executeUpdate(sql);
                    sql = "update deptt set deptt.pracflag=deptt.pracflag-1 where deptt.pracflag>0;";
                    stmt.executeUpdate(sql);
                }//loop timeslot
                
            }//loop day

            //STEP 6: Clean-up environment
            //rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try

        System.out.println("Goodbye!");
    }
}
